import os, pdb, sqlite3, subprocess
from functools import partial
from tqdm.notebook import tqdm
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
import statsmodels.api as sm

import sys
sys.path.append('/home/user/BuildSys/ecobee/refactored')
sys.path.append('/home/gbaasch/ecobee/refactored')
from exp.nb_methods import DecayCurves


def select_construction_layers(sql_path):
    conn = sqlite3.connect(sql_path)
    q = "SELECT C.ConstructionIndex, M.Thickness, M.Density, M.SpecHeat, S.SurfaceName, S.SurfaceIndex, S.Area, " \
        "M.Name as MaterialName, C.Name as ConstructionName FROM Constructions as C " \
        "JOIN Surfaces as S ON C.ConstructionIndex == S.ConstructionIndex " \
        "JOIN ConstructionLayers as CL on C.ConstructionIndex == CL.ConstructionIndex " \
        "JOIN Materials as M on CL.MaterialIndex == M.MaterialIndex "
    return pd.read_sql(q, conn)

# def get_capacitance(sql_path):
#     """ Return capacitance in [Ws/K] """
#     df = select_construction_layers(sql_path)
#     df['capacitance_material'] = (df['Thickness'] * df['Density'] * df['SpecHeat'] * df['Area'])
# #     pdb.set_trace()
# #     df = df[df['SurfaceName'] != 'FLOOR']

#     # Take the mean because there are multiple constructions for each surface
#     df2 = df.groupby('SurfaceName').mean()
# #     df2['capacitance'] = 1/((1/(df2['capacitance']/2))*2)
#     return df2['capacitance'].sum()


# def get_capacitance(sql_path):
#     """ Return capacitance in [Ws/K]
#     The material layers are joined in series and the whole envelope is joined in parallel

#     """
#     df = select_construction_layers(sql_path)
#     df['capacitance'] = (df['Thickness'] * df['Density'] * df['SpecHeat'] * df['Area'])

#     def series_capacitance(x):
#         return 1/(1/x).sum()
#     df_cap = df.groupby('SurfaceName').agg({'capacitance': [series_capacitance]})
#     return df_cap['capacitance'].sum().values[0]


def get_capacitance(sql_path):
    """ Return capacitance in [Ws/K]
    The material layers are joined in series and the whole envelope is joined in parallel
    
    """
    df = select_construction_layers(sql_path)
    df['capacitance'] = (df['Thickness'] * df['Density'] * df['SpecHeat'] * df['Area'])
    df_floor = df[df['SurfaceName'] == 'FLOOR']
    df_no_floor = df[df['SurfaceName'] != 'FLOOR']
    
    df_floor['capacitance'] = df_floor['capacitance'] / 2
    
    def series_capacitance(x):
        return 1/(1/x).sum()
    
    df_cap = df_no_floor.groupby('SurfaceName').agg({'capacitance': [series_capacitance]})
    return df_cap['capacitance'].sum().values[0] + df_floor['capacitance'].values[0]


class GrayBoxMethod():
    def __init__(self, dataset, dataset_name, idf, resample_rate, cols, save_dir=None):
        """
        param dataset: path to the dataset
        param save_dir: path to the location to save results
        """
        self.results       = []
        self.dataset       = dataset
        self.dataset_name  = dataset_name
        self.idf           = idf
        self.resample_rate = resample_rate
        self.cols          = cols
        try:
            self.buildings  = [x for x in os.listdir(dataset) if (dataset_name in x and '.csv' not in x)]
        except FileNotFoundError as e:
            print(f'WARNING: {dataset} not found')
        self.save_dir      = save_dir or 'result_csvs'
        self.save_file     = f'{self.save_prefix}_{self.resample_rate}_{self.dataset_name}.csv'
    
    def __call__(self):
        raise NotImplementedError()
    
    def get_timeseries(self):
        raise NotImplementedError()
    
    def get_model_hlc(self):
        raise NotImplementedError()
    
    def save_results(self):
        raise NotImplementedError()
        
    def get_zone_csv(self, b):
        return os.path.join(self.dataset, b, self.idf + '_1', 'THERMAL ZONE.csv')
    
    def get_sql(self, b):
        return os.path.join(self.dataset, b, self.idf + '_1', 'eplusout.sql')
    
    def save_results(self):
        save_dir  = os.path.join(self.save_dir, self.save_file)
        if not os.path.exists(self.save_dir): os.mkdir(self.save_dir)
        self.df_results = pd.DataFrame(self.results) 
        self.df_results.to_csv(save_dir, index=False)
    
    def append_rvalues(self, b, row):
        try:
            pth = os.path.join(self.dataset, b + '_5T.csv')
            ep_results = pd.read_csv(pth)
        
            rmat = 1/ep_results['envelope_umat'].values[0]
            rse  = 1/ep_results['envelope_use'].values[0]
            rsi  = 1/ep_results['envelope_usi'].values[0]
            row['hlc_mat'] = 1/rmat
            row['hlc_tot'] = 1/(rmat + rse + rsi)
            row['hlc_se']  = 1/rse
            row['hlc_si']  = 1/rsi
        except: pass
        return row
    
    def append_cvalues(self, b, row):
        sql_file = self.get_sql(b)
        row['c_tot'] = get_capacitance(sql_file)
        return row
    
    def append_infvalues(self, b, row):
        ca_air = 0.00121 * 1e6
        zone_csv = self.get_zone_csv(b)
        df = pd.read_csv(zone_csv, usecols=['infrate_standard','infrate_current'])
        row['hlc_inf_standard'] = df['infrate_standard'].mean() * ca_air
        row['hlc_inf_current']  = df['infrate_current'].mean()  * ca_air
        return row

# # ===================================
# # Balance Point
# # ======================================

class BalancePointMethod(GrayBoxMethod):
    def __init__(self, dataset, dataset_name, idf, resample_rate, cols, save_dir=None):
        self.save_prefix = 'bp'
        super().__init__(dataset, dataset_name, idf, resample_rate, cols, save_dir)
        
    def get_timeseries(self, b):
        zone_csv = self.get_zone_csv(b)
        df = pd.read_csv(
            zone_csv,
            parse_dates=['Date'], 
            index_col='Date', 
            usecols=['heating_rate', 'outtemp', 'Date',  'intemp', 'total_s']
        )
        df = df.resample(self.resample_rate).mean()
        df = df[df['heating_rate'] > 0]
        return df['2006-01-01': '2006-06-06']
    
    def fit_linear(self, b):
        df = self.get_timeseries(b)
        X  = df[self.cols]
        X  = sm.add_constant(X, has_constant='add') ## add an intercept (beta_0) to our model
        y  = df['heating_rate'] 
        model = sm.OLS(y, X).fit() 
        df  = model.conf_int().loc[self.cols]
        df['slope'] = model.params[self.cols]
        return df
    
    def get_model_hlc(self, b):
        fit = self.fit_linear(b)
        env_hlc = -fit.loc['outtemp']['slope']
        ub = -fit.loc['outtemp'][0]
        tot_hlc = env_hlc
        try:
            # only add ground R if it exists
            ground_hlc = -fit.loc['ground']['slope']
            tot_hlc    = ground_hlc + env_hlc
            err        = 0 # TODO not sure how to do this yet...
        except KeyError:
            ground_hlc = None
            err        = env_hlc-ub
        return tot_hlc, ground_hlc, env_hlc, err
    
    def __call__(self):
        print('Running Balance Points for all buildings')
        for b in tqdm(self.buildings):
            row = {'fname': b}
            row = self.append_rvalues(b, row)
            row = self.append_infvalues(b, row)
            tot_hlc_model, ground_hlc_model, env_hlc_model, err = self.get_model_hlc(b)
            row['hlc_tot_pred'] = tot_hlc_model
            row['pred_err']     = err
            # not using ground for now
            self.results.append(row)
        self.save_results()

method_params = {
    'generic': {
        'months': (10, 4),
        'hours': (20, 5),
        'timestep': 5,
        'T_in_col': 'intemp',
        'T_out_col': 'outtemp',
        'heat_col': 'heating_rate',
    },
    'decay_curves': {
        'proportion_heating': 0.1,
        'T_in_derivative_threshold': -0.3,
        'T_in_out_diff': 3,
        'minimum_intervals': 4
    },
}    


# # ===================================
# # Decay Curve
# # ======================================

class DecayCurveMethod(GrayBoxMethod):
    def __init__(self, dataset, dataset_name, idf, resample_rate, cols, 
                 save_dir=None, method_params=method_params):
        self.save_prefix = 'dc'
        super().__init__(dataset, dataset_name, idf, resample_rate, cols, save_dir)
        self.dc_model = DecayCurves(
            **method_params['generic'],
            **method_params['decay_curves'])
        
    def get_timeseries(self, b):
        zone_csv = self.get_zone_csv(b)
        return pd.read_csv(
            zone_csv,
            parse_dates=['Date'], 
            index_col='Date', 
            usecols=['heating_rate', 'outtemp', 'intemp', 'Date']
        )
    
    def get_model_hlc(self, b):
        df = self.get_timeseries(b)
        r = self.dc_model(df, 'THERMAL ZONE')
        if len(r) > 0:
            rdf = pd.DataFrame(r)
            rdf_no_outlier = rdf[rdf['tau_variance'] < 30]
            return rdf_no_outlier['tau'].mean()
        else: return None
        
    def __call__(self):
        print('Running Decay Curves for all buildings')
        for b in tqdm(self.buildings):
            row = {'fname'  : b}
            row = self.append_rvalues(b, row)
            row = self.append_cvalues(b, row)
            row = self.append_infvalues(b, row)
            tot_hlc_model = self.get_model_hlc(b)
            row['hlc_tot_pred'] = tot_hlc_model
            # row['pred_err']     = err # todo figure out error for decay curves
            self.results.append(row)
        self.save_results()

# # ===================================
# # RC
# # ======================================

class RCMethod(GrayBoxMethod):
    def __init__(self, dataset, dataset_name, idf, resample_rate, cols, 
                 mOrder, rc_result_dir, runner_csvs_dir, script_pth,
                 save_dir=None):
        """ RC Resample rates are formatted differently ex. jan7_5 """
        self.mOrder        = mOrder
        self.rc_result_dir = rc_result_dir
        self.save_prefix   = f'rc_{mOrder}'
        self.runner_csvs_dir = runner_csvs_dir
        self.script_pth      = script_pth
        super().__init__(dataset, dataset_name, idf, resample_rate, cols, save_dir)
        
    def run_rscript(self):
        # create input csv for R script
        rinput = {
            'base_dir': [self.dataset],
            'save_dir': [self.dataset_name],
            'datetime': [self.resample_rate],
            'model_id': [self.mOrder]
        }
        exp_name = f'experiments_{self.dataset_name}_{self.resample_rate}_{self.mOrder}.csv'
        csv_pth = os.path.join(self.runner_csvs_dir, exp_name)
        if not os.path.exists(self.runner_csvs_dir):
            os.mkdir(self.runner_csvs_dir)
        if not os.path.exists(self.rc_result_dir): 
            os.mkdir(self.rc_result_dir)
        df = pd.DataFrame.from_dict(rinput)
        df.to_csv(csv_pth, index=False)
        
        rscript_out_name = f'rscript_{self.dataset_name}_{self.resample_rate}_{self.mOrder}.out'
        
        with open(rscript_out_name, 'w+') as log:
            args = ["Rscript", os.path.join(self.script_pth, 'runMethods.R'), 
                 exp_name, self.runner_csvs_dir, self.rc_result_dir, self.script_pth]
            print(args)
            p = subprocess.Popen(args, stdout=log, stderr=log)
            p.wait()
        print('finished running')

    def get_model_hlc(self, rdir):
        agg_df = pd.read_csv(os.path.join(rdir, 'aggregated.csv'))
        fit_df = pd.read_csv(os.path.join(rdir, 'fit.csv'), index_col=0)
        hcl    = agg_df['hcl_actual'].values[0]
        loglik = agg_df['loglik'].values[0]
        hcl_ub = agg_df['hcl_ub'].values[0]
        # TODO - change the way C is retrieved depending on the order
        if self.mOrder == 'Ti2':
            # C are in [kWh/C]
            c = fit_df.loc['Ci']['estimates'] 
        else:
            c = (fit_df.loc['Ci']['estimates'], fit_df.loc['Ce']['estimates']) 
        return hcl, c, fit_df['estimates'].values, fit_df['probt'].values, fit_df.index.values, loglik, hcl_ub
           
    def __call__(self):
        print('Running RC Models R Script for all buildings...')
        self.run_rscript()
        print('Storing results...')
        self.save_from_rc_results()
        
    def save_from_rc_results(self):
        datasets = os.listdir(self.rc_result_dir)
        for dataset in datasets:
            if self.dataset_name == dataset:
                dataset_dir = os.path.join(self.rc_result_dir, dataset)
                for b in os.listdir(dataset_dir):
                    row = {'fname'  : b}
                    rc_result = f'{self.idf}_1_{self.resample_rate}_{self.mOrder}'
                    rc_result_dir = os.path.join(dataset_dir, b, rc_result)
                    try:
                        hlc_model, c_model, estimates, probt, names, loglik, hcl_ub = self.get_model_hlc(rc_result_dir)
                        row = self.append_rvalues(b, row)
                        row = self.append_cvalues(b, row)
                        row = self.append_infvalues(b, row)
                        row['hlc_tot_pred'] = hlc_model
                        row['c_tot_pred']   = c_model
                        row['estimates']    = estimates
                        row['probt']        = probt
                        row['names']        = names
                        row['loglik']       = loglik
                        row['err']          = hcl_ub - hlc_model
                        # TODO get model errors
                    except FileNotFoundError as e: print(e) #pass
                    self.results.append(row)
        self.save_results()

