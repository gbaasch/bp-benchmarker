cases_all = [
    ('victoria', 'control_concrete', False),
    ('victoria', 'control_concrete', True),
    ('victoria', 'control_wood', False),
    ('victoria', 'control_wood', True),
    ('chicago', 'control_concrete', False),
    ('chicago', 'control_concrete', True),
    ('chicago', 'control_wood', False),
    ('chicago', 'control_wood', True),
    ('victoria', 'control_concrete_inf', False),
    ('victoria', 'control_concrete_inf', True),
    ('victoria', 'control_wood_inf', False),
    ('victoria', 'control_wood_inf', True),
    ('chicago', 'control_concrete_inf', False),
    ('chicago', 'control_concrete_inf', True),
    ('chicago', 'control_wood_inf', False),
    ('chicago', 'control_wood_inf', True)
]

cases_no_infiltration = [
    ('victoria', 'control_concrete', False),
    ('victoria', 'control_concrete', True),
    ('victoria', 'control_wood', False),
    ('victoria', 'control_wood', True),
    ('chicago', 'control_concrete', False),
    ('chicago', 'control_concrete', True),
    ('chicago', 'control_wood', False),
    ('chicago', 'control_wood', True)
]

cases_w_infiltration = [
    ('victoria', 'control_concrete_inf', False),
    ('victoria', 'control_concrete_inf', True),
    ('victoria', 'control_wood_inf', False),
    ('victoria', 'control_wood_inf', True),
    ('chicago', 'control_concrete_inf', False),
    ('chicago', 'control_concrete_inf', True),
    ('chicago', 'control_wood_inf', False),
    ('chicago', 'control_wood_inf', True)
]


cases_w_infiltration_wood = [
    ('victoria', 'control_wood_inf', False),
    ('victoria', 'control_wood_inf', True),
    ('chicago', 'control_wood_inf', False),
    ('chicago', 'control_wood_inf', True)
]

cases_w_infiltration_concrete = [
    ('victoria', 'control_concrete_inf', False),
    ('victoria', 'control_concrete_inf', True),
    ('chicago', 'control_concrete_inf', False),
    ('chicago', 'control_concrete_inf', True),
]

cases_no_infiltration_wood = [
    ('victoria', 'control_wood', False),
    ('victoria', 'control_wood', True),
    ('chicago', 'control_wood', False),
    ('chicago', 'control_wood', True)
]


cases_no_infiltration_concrete = [
    ('victoria', 'control_concrete', False),
    ('victoria', 'control_concrete', True),
    ('chicago', 'control_concrete', False),
    ('chicago', 'control_concrete', True),
]


