#!/bin/bash
#SBATCH --account=rpp-revins

#SBATCH --array=1-800
#SBATCH --time=02:00:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=5000M
#SBATCH --mail-user=gbaasch@uvic.ca
#SBATCH --mail-type=ALL


#!generate the virtual environment
module load python/3.6

source ~/envs/dl3.6/bin/activate

echo "prog started at: `date`"
echo "task: `$SLURM_ARRAY_TASK_ID`"

srun python generate_data.py -task_id $SLURM_ARRAY_TASK_ID -samples_per_task 1 -inputs inputs_train.csv -dataset_path /scratch/gbaasch/bp_benchmark_data/train -schedule_suffix _train -schedule_dir /scratch/gbaasch/schedules

deactivate
echo "prog ended at: `date`"

