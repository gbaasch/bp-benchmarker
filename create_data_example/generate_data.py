import sys, os

sys.path.append('../data_creation')

import argparse
import matplotlib.pyplot as plt
import pandas as pd

from BuildingData import *
from BuildingDataCreator import *
from ParamController import *
from EPlusResults import *


#################################################################################################
# ##########################   Set up argparser here  ############################################
# ################################################################################################

parser = argparse.ArgumentParser(description='Process the Job IDs to retrieve the inputs.')
parser.add_argument('-task_id', type=int, nargs='+',
                   help='Task id to pick inputs')
parser.add_argument('-samples_per_task', type=int, nargs='+',
                   help='Number of samples to run per task.')
parser.add_argument('-inputs', type=str, nargs='+', help='The inputs filename')
parser.add_argument('-dataset_path', type=str, nargs='+', help='The root data director')
parser.add_argument('-schedule_suffix', type=str, nargs='+', help='The suffix for the equip and occ directory names')
parser.add_argument('-schedule_dir', type=str, nargs='+', help='Root schedule directory')
args = parser.parse_args()

schedule_dir = args.schedule_dir[0]
schedule_suffix = args.schedule_suffix[0]
inputs = args.inputs[0]
dataset_path = args.dataset_path[0]
#inputs_path = os.path.join(dataset_path, inputs)

print("Running with args: ", args.task_id[0], args.samples_per_task[0], inputs, schedule_suffix, dataset_path)

IDF_DIR = 'idfs'
EPW_DIR = 'epws'

epws   = ['victoria.epw', 'chicago.epw']                      # weather
idfs   = ['control_concrete.idf', 'control_wood.idf',         # envelope
          'control_concrete_inf.idf', 'control_wood_inf.idf'] # infiltration
scheds = [True, False]                                        # schedules


eplus_path   = '/home/gbaasch/projects/def-revins/gbaasch/EnergyPlus92/' 


for epw in epws:
    fepw = os.path.join(EPW_DIR, epw)
    for idf in idfs:
        for sched in scheds:
            save_dir  = epw[:-4] + '_' + idf[:-4] + '_sched' + str(sched) + '_job' + str(args.task_id[0])
            save_path = os.path.join(dataset_path, save_dir)
            bdc = BuildingDataCreator(
                    IDF_DIR, idf, save_path, 
                    ElectricPowerDynamicHLC, run_surfaces=False, add_schedules=sched, epw=fepw, 
                    schedule_suffix=schedule_suffix, schedule_dir=schedule_dir,
                    ep_path=eplus_path, outdir=f'outputdir_{schedule_suffix}/outputdir_{args.task_id[0]}')
            
            # this will create the input file if it does not exist already, or use it if it does
            bdc.run_from_inputs_file(inputs, EnvelopeControllerRealisticBoth, args.task_id[0], args.samples_per_task[0], 1000)
            # save the dynamic heat loss coefficient
            print('Save dynamic heat loss coefficient')
            epresults = EPlusResults(os.path.join(dataset_path, save_dir), idf[:-4], '5T')
            epresults.load()
            epresults.save(os.path.join(dataset_path, f'{save_dir}_5T.csv'))
            print('\n')
