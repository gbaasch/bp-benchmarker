import argparse, os

import papermill as pm

parser = argparse.ArgumentParser(description='Process the Job IDs to retrieve the inputs.')
parser.add_argument('-network', type=str, nargs='+')
parser.add_argument('-label_idx', type=int, nargs='+')
parser.add_argument('-train_fname', type=str, nargs='+')
parser.add_argument('-valid_fname', type=str, nargs='+')
args = parser.parse_args()

network = args.network[0]
label_idx = args.label_idx[0]
train_fname = args.train_fname[0]
valid_fname = args.valid_fname[0]

output = os.path.join('papermill_outputs', f'{network}_label{label_idx}.ipynb')

pm.execute_notebook(
   'run_neural_nets.ipynb',
   output,
   parameters=dict(network=network, label_idx=label_idx, train_fname=train_fname, valid_fname=valid_fname)
)
