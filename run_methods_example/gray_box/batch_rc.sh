#!/bin/bash

#SBATCH --account=rpp-revins

#SBATCH --array=0-95
#SBATCH --time=03:30:00
#SBATCH --cpus-per-task=1
#SBATCH --mem=2000mb
#SBATCH --mail-user=gbaasch@uvic.ca
#SBATCH --mail-type=ALL


# prepare R
export R_LIBS=~/local/R_libs/
module load gcc/7.3.0 r/3.6.0

# load the virtual environment
source ~/envs/dl3.6/bin/activate


echo "prog started at: `date`"


case $SLURM_ARRAY_TASK_ID in
    0) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_concrete -sched True" ;;
    1) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_concrete -sched False" ;;
    2) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_wood -sched True" ;;
    3) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_wood -sched False" ;;
    4) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_concrete_inf -sched True" ;;
    5) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_concrete_inf -sched False" ;;
    6) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_wood_inf -sched True" ;;
    7) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw victoria -idf control_wood_inf -sched False" ;;
    8) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_concrete -sched True" ;;
    9) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_concrete -sched False" ;;
    10) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_wood -sched True" ;;
    11) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_wood -sched False" ;;
    12) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_concrete_inf -sched True" ;;
    13) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_concrete_inf -sched False" ;;
    14) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_wood_inf -sched True" ;;
    15) ARGS="-mOrder Ti2 -dt dt_jan7_5 -epw chicago -idf control_wood_inf -sched False" ;;
    
    16) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_concrete -sched True" ;;
    17) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_concrete -sched False" ;;
    18) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_wood -sched True" ;;
    19) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_wood -sched False" ;;
    20) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_concrete_inf -sched True" ;;
    21) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_concrete_inf -sched False" ;;
    22) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_wood_inf -sched True" ;;
    23) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw victoria -idf control_wood_inf -sched False" ;;
    24) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_concrete -sched True" ;;
    25) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_concrete -sched False" ;;
    26) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_wood -sched True" ;;
    27) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_wood -sched False" ;;
    28) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_concrete_inf -sched True" ;;
    29) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_concrete_inf -sched False" ;;
    30) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_wood_inf -sched True" ;;
    31) ARGS="-mOrder TiTe1 -dt dt_jan7_5 -epw chicago -idf control_wood_inf -sched False" ;;
    
    32) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_concrete -sched True" ;;
    33) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_concrete -sched False" ;;
    34) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_wood -sched True" ;;
    35) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_wood -sched False" ;;
    36) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_concrete_inf -sched True" ;;
    37) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_concrete_inf -sched False" ;;
    38) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_wood_inf -sched True" ;;
    39) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw victoria -idf control_wood_inf -sched False" ;;
    40) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_concrete -sched True" ;;
    41) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_concrete -sched False" ;;
    42) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_wood -sched True" ;;
    43) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_wood -sched False" ;;
    44) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_concrete_inf -sched True" ;;
    45) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_concrete_inf -sched False" ;;
    46) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_wood_inf -sched True" ;;
    47) ARGS="-mOrder TiTeInf -dt dt_jan7_5 -epw chicago -idf control_wood_inf -sched False" ;;
    
    48) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_concrete -sched True" ;;
    49) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_concrete -sched False" ;;
    50) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_wood -sched True" ;;
    51) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_wood -sched False" ;;
    52) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_concrete_inf -sched True" ;;
    53) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_concrete_inf -sched False" ;;
    54) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_wood_inf -sched True" ;;
    55) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw victoria -idf control_wood_inf -sched False" ;;
    56) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_concrete -sched True" ;;
    57) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_concrete -sched False" ;;
    58) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_wood -sched True" ;;
    59) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_wood -sched False" ;;
    60) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_concrete_inf -sched True" ;;
    61) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_concrete_inf -sched False" ;;
    62) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_wood_inf -sched True" ;;
    63) ARGS="-mOrder Ti2 -dt dt_jan14_5 -epw chicago -idf control_wood_inf -sched False" ;;
    
    64) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_concrete -sched True" ;;
    65) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_concrete -sched False" ;;
    66) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_wood -sched True" ;;
    67) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_wood -sched False" ;;
    68) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_concrete_inf -sched True" ;;
    69) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_concrete_inf -sched False" ;;
    70) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_wood_inf -sched True" ;;
    71) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw victoria -idf control_wood_inf -sched False" ;;
    72) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_concrete -sched True" ;;
    73) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_concrete -sched False" ;;
    74) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_wood -sched True" ;;
    75) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_wood -sched False" ;;
    76) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_concrete_inf -sched True" ;;
    77) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_concrete_inf -sched False" ;;
    78) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_wood_inf -sched True" ;;
    79) ARGS="-mOrder TiTe1 -dt dt_jan14_5 -epw chicago -idf control_wood_inf -sched False" ;; 
    
    80) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_concrete -sched True" ;;
    81) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_concrete -sched False" ;;
    82) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_wood -sched True" ;;
    83) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_wood -sched False" ;;
    84) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_concrete_inf -sched True" ;;
    85) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_concrete_inf -sched False" ;;
    86) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_wood_inf -sched True" ;;
    87) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw victoria -idf control_wood_inf -sched False" ;;
    88) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_concrete -sched True" ;;
    89) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_concrete -sched False" ;;
    90) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_wood -sched True" ;;
    91) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_wood -sched False" ;;
    92) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_concrete_inf -sched True" ;;
    93) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_concrete_inf -sched False" ;;
    94) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_wood_inf -sched True" ;;
    95) ARGS="-mOrder TiTeInf -dt dt_jan14_5 -epw chicago -idf control_wood_inf -sched False" ;;
esac

srun python run_all_rc.py $ARGS -dataset_path /scratch/gbaasch/bp_benchmark_data/test
