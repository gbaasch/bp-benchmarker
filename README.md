# Building Property Benchmarker

A toolset for the development and benchmarking of computational approaches for building property estimation.


This repository is under development. It has an associated journal paper that is currently undergoing revisions. More information and details about the code will be posted alongside the paper upon publication. To save memory, the data is currently in hdf5 format. Please refer to the data_creation/CreateHDF5.py file for information on how the hdf5 files are formatted. 
