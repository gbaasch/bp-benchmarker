import pandas as pd 

from besos.parameters import expand_plist, RangeParameter, FieldSelector, Parameter, CategoryParameter
from besos import sampling


class ParamController():
    def __init__(self, building, sampletype=sampling.lhs):
        """ Building is an Eppy object whose parameters will be changed """
        self.building   = building
        self.sampletype = sampletype
    
    def alter_params(self):
        raise NotImplementedError('')

        

# ## ==============================
# ## Sensitivity Analysis Parameters
# ## ==============================


# https://www.pnnl.gov/main/publications/external/technical_reports/PNNL-18898.pdf
infiltration_param = Parameter(
    value_descriptor=RangeParameter(min_val = 0.0, max_val=0.00915),  
    selector=FieldSelector(class_name='ZoneInfiltration:DesignFlowRate', 
                           object_name='Infiltration',
                           field_name='Flow per Exterior Surface Area')
)

# https://knowledge.autodesk.com/support/revit-products/getting-started/caas/simplecontent/content/equipment-and-lighting-loads.html
light_load_param = Parameter(
    value_descriptor=RangeParameter(min_val = 0.00, max_val=5),  
    selector=FieldSelector(class_name='Lights', 
                           object_name='Lighting Load',
                           field_name='Watts per Zone Floor Area')
)

# I just chose this randomly
people_load_param = Parameter(
    value_descriptor=RangeParameter(min_val = 0.0, max_val=5),  
    selector=FieldSelector(class_name='People', 
                           object_name='People Load',
                           field_name='People per Zone Floor Area')
)

# https://knowledge.autodesk.com/support/revit-products/getting-started/caas/simplecontent/content/equipment-and-lighting-loads.html
equipment_load_param = Parameter(
    value_descriptor=RangeParameter(min_val = 0.0, max_val=55),  
    selector=FieldSelector(class_name='ElectricEquipment', 
                           object_name='Equipment Load',
                           field_name='Watts per Zone Floor Area')
)

    

### ==============================
### Cases for Sensitivity Analysis
### ==============================
class InfiltrationController(ParamController):
    def __init__(self, building, **kwargs):
        super().__init__(building, **kwargs)
        self.params = [
            infiltration_param
        ]


class InternalLoadsController(ParamController):
    def __init__(self, building, **kwargs):
        super().__init__(building, sampletype=sampling.random)
        self.params = [
            equipment_load_param,
            light_load_param,
            people_load_param
        ]


class OccVentController(ParamController):
    def __init__(self, building):
        super().__init__(building, sampletype=sampling.random)
        for oa in building.idfobjects['DesignSpecification:OutdoorAir']:
            oa.Outdoor_Air_Method = 'Flow/Person'
            # from https://www.engineeringtoolbox.com/ventilation-air-flow-rate-d_115.html
            oa.Outdoor_Air_Flow_per_Person = 0.015
        
        self.params = [
            people_load_param
        ]


### ==============================
### Envelop Analysis Parameters
### ==============================
# GYPSUM
# https://www.buildsite.com/pdf/nationalgypsum/Gypsum-Construction-Guide-Gypsum-Wallboard-Systems-Product-Catalog-B23966.pdf
gypsum_param = Parameter(
    name="1/2IN Gypsum Thickness",
    value_descriptor=RangeParameter(min_val = 0.00633, max_val=0.0159),  
    selector=FieldSelector(class_name='Material', 
                           object_name='1/2IN Gypsum',
                           field_name='Thickness'))

# STUCCO
# https://web.ornl.gov/sci/buildings/conf-archive/2004%20B9%20papers/126_Brown.pdf
stucco_param = Parameter(
    name="1IN Stucco Thickness",
    value_descriptor=RangeParameter(min_val = 0.015, max_val=0.030),
    selector=FieldSelector(class_name='Material', 
                           object_name='1IN Stucco',
                           field_name='Thickness'))

# CONCRETE
# http://www.buildingcode.online/1623.html
concrete_param = Parameter(
    name="8IN Concrete HW Thickness",
    value_descriptor=RangeParameter(min_val = 0.2, max_val=0.3),
    selector=FieldSelector(class_name='Material', 
                           object_name='8IN Concrete HW',
                           field_name='Thickness'))


# WOOD
# https://cwc.ca/wp-content/uploads/2019/03/Plywood-Sizes.pdf
wood_param = Parameter(
    name="Plywood1/2 in",
    value_descriptor=RangeParameter(min_val = 0.006, max_val=0.03),
    selector=FieldSelector(class_name='Material', 
                           object_name='Plywood1/2 in',
                           field_name='Thickness'))

# ROOF INSULATION
# https://www.uswitch.com/insulation/guides/how-to-insulate-a-loft/
roofins_param = Parameter(
    name="Roof Insulation [18] Thickness",
    value_descriptor=RangeParameter(min_val = 0.1, max_val=0.3),
    selector=FieldSelector(class_name='Material', 
                           object_name='Roof Insulation [18]',
                           field_name='Thickness'))

# ROOF MEMBRANE
# https://www.rubber4roofs.co.uk/how-thick-is-the-membrane
 # 0.0095 was in energy plus originally
roofmem_param = Parameter(
    name="Roof Membrane Thickness",
    value_descriptor=RangeParameter(min_val = 0.0012, max_val=0.0095),
    selector=FieldSelector(class_name='Material', 
                           object_name='Roof Membrane',
                           field_name='Thickness'))

# ROOF METAL DECKING
# https://ascsd.com/files/RoofDeck.pdf
roofdeck_param = Parameter(
    name="Metal Decking Thickness",
    value_descriptor=RangeParameter(min_val = 0.0007, max_val=0.0015),
    selector=FieldSelector(class_name='Material', 
                           object_name='Metal Decking',
                           field_name='Thickness'))

# WALL INSULATION
# https://www.energy.gov/energysaver/weatherize/insulation/types-insulation
# minimum value chosen from energy plus
wallins_param = Parameter(
    name="Wall Insulation [31] Thickness",
    value_descriptor=RangeParameter(min_val = 0.035, max_val=0.3048),  
    selector=FieldSelector(class_name='Material', 
                           object_name='Wall Insulation [31]',
                           field_name='Thickness'))

# WINDOW GLAZING
# http://www.replacedoubleglazing.com/double%20glazing.pdf
window_glazing_param = Parameter(
    name="CLEAR 3MM Thickness",
    value_descriptor=RangeParameter(min_val = 0.001, max_val=0.01),
    selector=FieldSelector(class_name='WindowMaterial:Glazing', 
                           object_name='CLEAR 3MM',
                           field_name='Thickness'))


# WINDOW AIRGAP
# Chose according to http://www.replacedoubleglazing.com/double%20glazing.pdf
window_air_param = Parameter(
    name="AIR 6MM Thickness",
    value_descriptor=RangeParameter(min_val = 0.006, max_val=0.02),
    selector=FieldSelector(class_name='WindowMaterial:Gas', 
                           object_name='AIR 6MM',
                           field_name='Thickness'))


### ==============================
### Envelope Cases for Black Box
### ==============================   
class EnvelopeControllerRealisticWood(ParamController):
    def __init__(self, building, **kwargs):
        super().__init__(building, **kwargs)
        # TODO WWR
        # TODO - ignored floor for now b/c using adabiatic
        self.params   = [
            gypsum_param,
            stucco_param,
            wood_param,
            roofins_param,
            roofmem_param,
            roofdeck_param,
            wallins_param,
            window_glazing_param,
            window_air_param
        ]

class EnvelopeControllerRealistic(ParamController):
    def __init__(self, building, **kwargs):
        super().__init__(building, **kwargs)
        # TODO WWR
        # TODO - ignored floor for now b/c using adabiatic
        self.params   = [
            gypsum_param,
            stucco_param,
            concrete_param,
            roofins_param,
            roofmem_param,
            roofdeck_param,
            wallins_param,
            window_glazing_param,
            window_air_param
        ]


class EnvelopeControllerRealisticBoth(ParamController):
    def __init__(self, building, **kwargs):
        super().__init__(building, **kwargs)
        # TODO WWR
        # TODO - ignored floor for now b/c using adabiatic
        self.params   = [
            gypsum_param,
            stucco_param,
            concrete_param,
            wood_param,
            roofins_param,
            roofmem_param,
            roofdeck_param,
            wallins_param,
            window_glazing_param,
            window_air_param
        ]


class EnvelopeController(ParamController):
    def __init__(self, building, **kwargs):
        super().__init__(building, **kwargs)
        # TODO WWR
        wall_params   = self.wall_params()
        window_params = self.window_params()
        self.params = wall_params + window_params
           
    def wall_params(self):
        params = []
        for material in self.building.idfobjects['MATERIAL']:
            wall_params = {
                # Chosen according to the min and max values in the idf.
                # They may not necessarily represent reality
                material.Name    : {
                    'Thickness'    : (0.0008,0.2), 
                    'Density'      : (43,7823), 
                    'Specific Heat': (418,1460)
                }}
            params = params + expand_plist(wall_params)
        return params    
    
    def window_params(self):
        params = []
        for glazing in self.building.idfobjects['WINDOWMATERIAL:Glazing']:
            glazing_params = {
                glazing.Name : {
                    # Chose thicknes so mean is in 
                    #  http://www.replacedoubleglazing.com/double%20glazing.pdf
                    # Chose conductivity so mean is conductivity of glass
                    'Thickness'    : (0.001,0.01),
                    'Conductivity' : (0.4,1.5),   
                }}
            params = params + expand_plist(glazing_params)
        for window_air in self.building.idfobjects['WindowMaterial:Gas']:
            window_air_params = {
                window_air.Name : {
                    # Chose according to http://www.replacedoubleglazing.com/double%20glazing.pdf
                    'Thickness': (0.006,0.02), 
                }}
            params = params + expand_plist(window_air_params)
        return params
