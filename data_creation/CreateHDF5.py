import re, h5py, os, pdb, argparse

import numpy as np
import pandas as pd

from sys import getsizeof


# +
parser = argparse.ArgumentParser(description='Process parameters')
parser.add_argument('-root_train', type=str, nargs='+',help='Root training data directory')
parser.add_argument('-root_test', type=str, nargs='+', help='Root testing data directory')
parser.add_argument('-save_dir', type=str, nargs='+', help='Directory to save resulting hdf5 files')
args = parser.parse_args()

root_train = args.root_train[0]
root_test  = args.root_test[0]

save_dir   = args.save_dir[0]
# -

if not os.path.exists(save_dir):
    os.mkdir(save_dir)

epws   = ['victoria', 'chicago']                      # weather
idfs   = ['control_concrete', 'control_wood',         # envelope
          'control_concrete_inf', 'control_wood_inf'] # infiltration
scheds = [False, True]

ca_air = 0.00121 * 1e6


# +
def append_data(root, labels, ts, md, idf, epw, sched, b):
    try:

        labels_csv = os.path.join(root, b + '_5T.csv')
        ep_results = pd.read_csv(labels_csv, usecols=['envelope_umat', 'envelope_use', 'envelope_usi']).values[0]               
        
        zone_csv = os.path.join(root, b, idf + '_1', 'THERMAL ZONE.csv')
        building_ts = pd.read_csv(zone_csv, usecols=['heating_rate', 'intemp', 'outtemp', 'total_s'], nrows=2000).values
        building_inf = pd.read_csv(zone_csv, usecols=['infrate_current'], nrows=2000)
        
        rmat = 1/ep_results[0]
        rse  = 1/ep_results[1]
        rsi  = 1/ep_results[2]
        hlc_tot = 1/(rmat + rse + rsi)
        
        hlc_inf = building_inf['infrate_current'].mean()  * ca_air
        building_labels = [hlc_tot, 1/rmat, 1/rse, 1/rsi, hlc_inf, hlc_tot + hlc_inf]
        
        # save building metadata
        job_id = re.match('.+_(job\d+)$', b).group(1)
        building_md = [epw, idf, str(sched), job_id]

        # Only append after we know all the necessary files were found
        ts.append(building_ts)
        md.append(building_md) 
        labels.append(building_labels)
    except FileNotFoundError as e:
        pass


def load_lists(root):
    labels = []
    ts = []
    md = []

    for epw in epws:
        for idf in idfs:
            for sched in scheds:
                dataset_name  = epw + '_' + idf + '_sched' + str(sched)
                buildings  = [x for x in os.listdir(root) if (dataset_name in x and '.csv' not in x)]
                print(len(buildings))
                for building in buildings:
                    append_data(root, labels, ts, md, idf, epw, sched, building)
                    
    return labels, ts, md

def arrays2hd5(ts, labels, md, fname):
    ts = np.array(ts)
    labels = np.array(labels)
    md_encoded = md.copy()

    for i, r in enumerate(md):
        md_encoded[i] = [n.encode("ascii", "ignore") for n in r]

    with h5py.File(fname, 'w') as f:
        f.create_dataset("md", data=md_encoded, dtype='S20')
        f.create_dataset("ts", data=ts)
        f.create_dataset("labels", data=labels)  


# -

labels_train, ts_train, md_train = load_lists(root_train)
labels_test, ts_test, md_test = load_lists(root_test)

arrays2hd5(ts_train, labels_train, md_train, os.path.join(save_dir, 'train.hdf5'))
arrays2hd5(ts_test, labels_test, md_test, os.path.join(save_dir, 'test.hdf5'))
