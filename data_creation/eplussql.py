import pdb, time
from functools import lru_cache
import pandas as pd
import sqlite3


# Connect to SQL
sql_path = 'eplusout.sql'
conn = sqlite3.connect(sql_path)

# helper functions
def gj_to_kwh(gj):
    return gj * 277.8

def j_to_kwh(j):
    return j * 2.77778e-7

def get_total_floor_area():
    z = select_all_from_table('Zones')
    return z['FloorArea'].sum()

def get_zones(sql_path):
    conn = get_conn(sql_path)
    q = "SELECT ZoneName FROM Zones"
    return pd.read_sql(q, conn).values

def get_surfaces(sql_path):
    conn = get_conn(sql_path)
    q = "SELECT SurfaceName FROM Surfaces"
    return pd.read_sql(q, conn).values

# @lru_cache(maxsize=16)
def get_conn(sql_path):
    return sqlite3.connect(sql_path) 

# Define Queries
def select_all_from_table(sql_path, table_name):
    conn = get_conn(sql_path)
    q = "SELECT * FROM {}".format(table_name)
    return pd.read_sql(q, conn)

def select_tabular_report(sql_path, report_name, table_name=None):
    conn = get_conn(sql_path)
    q = "SELECT * FROM TabularDataWithStrings WHERE ReportName='{}'".format(report_name)
    if table_name:
        q = q + " and TableName='{}'".format(table_name)
    return pd.read_sql(q, conn)

def select_tables(sql_path):
    conn = get_conn(sql_path)
    query = "SELECT name FROM sqlite_master WHERE type='table';"
    return pd.read_sql(query ,conn)


reporting_frequencies = ['Zone Timestep', 'Hourly', 'Daily', 'Monthly']
index_groups = ['Facility:DistrictHeating', 'Facility:Electricity', 'Zone', 'Facility:DistrictCooling']


def zero_index_minutes(row):
    minute = row['Minute']
    hour = row['Hour']
    
    minute_new = minute
    return row


def index_time_series(df, frequency):
    if frequency == 'Monthly':
        df['Date'] = pd.to_datetime(
            df['Year'].astype(str) + '/' + 
            df['Month'].astype(str) + 
            '/01'
        )
    elif frequency == 'Daily':
        df['Date'] = pd.to_datetime(
            df['Year'].astype(str) + '/' + 
            df['Month'].astype(str) + '/' + 
            df['Day'].astype(str)
        )
    elif frequency == 'Hourly':
        df['Hour'] = df['Hour'] - 1
        df['Date'] = pd.to_datetime(
            df['Year'].astype(str) + '/' + 
            df['Month'].astype(str) + '/' + 
            df['Day'].astype(str) + ' ' +
            df['Hour'].astype(str) + ':00'
        )
    else:  
        step = df['Minute'][0]
        df['Minute'] = df['Minute'] - step
        df.loc[df.Minute < 0, 'Hour'] = df.loc[df.Minute < 0, 'Hour'] - 1
        df.loc[df.Minute < 0, 'Minute'] = 60 - step
        
        df['Date'] = pd.to_datetime(
            df['Year'].astype(str) + '/' + 
            df['Month'].astype(str) + '/' + 
            df['Day'].astype(str) + ' ' +
            df['Hour'].astype(str) + ':' +
            df['Minute'].astype(str)
        )
    
    return df['Date']


s = 'Value, Name, IndexGroup, KeyValue, Units, Year, Month, Day, Hour, Minute'
# s = '*'

def select_time_series(sql_path, reporting_frequency, index_group=None, name=None, name_list=None, convert_j=True, select_vals=s):
    conn = get_conn(sql_path)
    q = f"SELECT {select_vals} FROM ReportData as RD " \
        "JOIN ReportDataDictionary as RDD on RD.ReportDataDictionaryIndex = RDD.ReportDataDictionaryIndex " \
        "JOIN Time as T on T.TimeIndex = RD.TimeIndex " \
        "AND RDD.ReportingFrequency == '{}' "
    if index_group:
        q = q + "AND IndexGroup == '{}' ".format(index_group)
    if name:
        q = q + "AND Name == '{}' ".format(name)
    if name_list:
        q = q + "AND Name IN ({}) ".format(name_list)
    
    q = q.format(reporting_frequency)
    
    df = pd.read_sql(q, conn)
    st = time.time()
    df.index = index_time_series(df, reporting_frequency)
#     print('indexing', time.time() - st)
    
    df.Value = pd.to_numeric(df.Value)

    # TODO !!!! CONSIDER LATER
#     if convert_j:
#         df.loc[df.Units == 'J', 'Value'] = df.loc[df.Units == 'J', 'Value'].apply(j_to_kwh)
#         df.loc[df.Units == 'J', 'Units'] = 'kWh'
    return df[['Units', 'Value', 'KeyValue', 'Name']]
