import os, traceback, time

import matplotlib.pyplot as plt
import pandas as pd
from besos import eppy_funcs as ef
from eplussql import *

### ================================================
### Possible Variables for Consideration
### ================================================
step                 = 'Zone Timestep'                                        # 5 minutes 
heating_name         = 'Zone Air System Sensible Heating Rate'                # [W]
cooling_name         = 'Zone Air System Sensible Cooling Rate'                # [W]
intemp_name          = 'Zone Air Temperature'                                 # [degrees C]
lights_name          = 'Zone Lights Electric Power'                           # [W]
equip_name           = 'Zone Electric Equipment Electric Power'               # [W]
occ_name             = 'Zone People Occupant Count'
infloss_name         = 'Zone Infiltration Total Heat Loss Energy'             # [J]
infgain_name         = 'Zone Infiltration Total Heat Gain Energy'             # [J]
infmass_name         = 'Zone Infiltration Mass'                               # [kg]
heatstpt_name        = 'Zone Infiltration Total Heat Loss Energy'             # [C]
coolstpt_name        = 'Zone Infiltration Total Heat Gain Energy'             # [C]
ventchngs_name       = 'Zone Mechanical Ventilation Air Changes per Hour'     # [ach]
windowgain_name      = 'Zone Windows Total Heat Gain Rate'                    # [W]
windowloss_name      = 'Zone Windows Total Heat Loss Rate'                    # [W]
#### Weather
outtemp_name         = 'Site Outdoor Air Drybulb Temperature'                 # [degrees C]
diffuse_name         = 'Site Diffuse Solar Radiation Rate per Area'           # [W/m2]
direct_name          = 'Site Direct Solar Radiation Rate per Area'            # [W/m2]
ground_name          = 'Site Ground Reflected Solar Radiation Rate per Area'  # [W/m2]


zone_sql_measurements = {
    'heating_rate'   : 'Zone Air System Sensible Heating Rate',                  # [W]
    'cooling_rate'   : 'Zone Air System Sensible Cooling Rate',                  # [W]
    'intemp'         : 'Zone Air Temperature',                                   # [degrees C]  
    'lights_power'   : 'Zone Lights Electric Power',                             # [W]       
    'equip_power'    : 'Zone Electric Equipment Electric Power',                 # [W]  
    'occ_count'      : 'Zone People Occupant Count',                                    
    'infloss_energy' : 'Zone Infiltration Total Heat Loss Energy',               # [J]   
    'infgain_energy' : 'Zone Infiltration Total Heat Gain Energy',               # [J]   
    'infmass'        : 'Zone Infiltration Mass',                                 # [kg]  
    'infrate_current': 'Zone Infiltration Current Density Volume Flow Rate',     # [m3/s]
    'infrate_standard':'Zone Infiltration Standard Density Volume Flow Rate',    # [m3/s]
    'ventchngs_ach'  : 'Zone Mechanical Ventilation Air Changes per Hour',       # [ach]           
    'wndwgain_rate'  : 'Zone Windows Total Heat Gain Rate',                      # [W]              
    'wndwloss_rate'  : 'Zone Windows Total Heat Loss Rate',                      # [W]              
    'occ_power'      : 'Zone People Total Heating Rate',                         # [W]   
    'temp_star'      : 'Zone Air Heat Balance Surface Convection Rate',          # [W]   
    
    ## The following are not per zone, they are the facility total
    # TODO create new dict when using multiple zones
    'totalelec_power': 'Facility Total Electric Demand Power',                   # [W]
    'totalhvac_power': 'Facility Total HVAC Electric Demand Power',              # [W]      
    'totalbuil_power': 'Facility Total Building Electric Demand Power',          # [W]
}

site_sql_measurements = {
    'outtemp'        : 'Site Outdoor Air Drybulb Temperature',                                 
    'diffuse_rate'   : 'Site Diffuse Solar Radiation Rate per Area',                  
    'direct_rate'    : 'Site Direct Solar Radiation Rate per Area' ,                  
    'ground_rate'    : 'Site Ground Reflected Solar Radiation Rate per Area',
    'ground_temp'    : 'Site Ground Temperature',
    'ground_stemp'   : 'Site Surface Ground Temperature',
    'ground_dtemp'   : 'Site Deep Ground Temperature',
    'ground_factor'  : 'Site Simple Factor Model Ground Temperature'
}

surface_sql_measurements = {
    'inside_temp'    : 'Surface Inside Face Temperature',                                             # [C]
    'hgr_conv'       : 'Surface Inside Face Convection Heat Gain Rate',                               # [W]
    'hgr_rad'        : 'Surface Inside Face Net Surface Thermal Radiation Heat Gain Rate',            # [W]
    'hgr_cond_loss'  : 'Surface Inside Face Conduction Heat Loss Rate',
    'htc_conv'       : 'Surface Outside Face Convection Heat Transfer Coefficient',                   # [W/m2-K]
    'htc_rad_sky'    : 'Surface Outside Face Thermal Radiation to Sky Heat Transfer Coefficient',     # [W/m2-K]
    'htc_rad_air'    : 'Surface Outside Face Thermal Radiation to Air Heat Transfer Coefficient',     # [W/m2-K]
    'htc_rad_grd'    : 'Surface Outside Face Thermal Radiation to Ground Heat Transfer Coefficient',  # [W/m2-K]
    'htc_conv_in'    : 'Surface Inside Face Convection Heat Transfer Coefficient',                    # [W/m2-K]
    'outside_temp'   : 'Surface Outside Face Temperature'                                             # [C]
}


# ## ================================================
# ## Base Class, extensions are below
# ## ================================================  

## TODO - UNIT CONVERSION! LOOK BACK AT SQL SELECTION - select_time_series might do unit conversion
class BuildingData():
    def __init__(self, sqlfile, run_surfaces):
        self.sqlfile          = sqlfile
        self.run_surfaces      = run_surfaces
        self.zones            = None
        self.zone_sql_dfs     = {}
        self.site_sql_dfs     = {}
        self.surface_sql_dfs  = {}
        self.zone_datasets    = {}
        self.surface_datasets = {}
        self.zone_output_vars = [
            'heating_rate', 'intemp', 'temp_star', 
        ]
        self.site_output_vars = [
            'outtemp',
            'diffuse_rate', 'direct_rate','ground_rate',
#           'ground_factor',
#             'ground_temp', 'ground_stemp', 'ground_dtemp', 
        ]
        self.surface_output_vars = []
        self.surface_sql_measurements = []
        eplus_zone_output_vars = [zone_sql_measurements[x] for x in self.zone_output_vars]
        eplus_site_output_vars = [site_sql_measurements[x] for x in self.site_output_vars]
        self.eplus_output_vars = eplus_zone_output_vars + eplus_site_output_vars
        
        
    def load(self):
        # TODO run energy plus
        try:
            print(self.sqlfile)
            self.zones = get_zones(self.sqlfile)
            if self.run_surfaces: self.surfaces = get_surfaces(self.sqlfile)
            self.get_output_variables()
            self.get_weather_dataset()
            if self.run_surfaces: self.get_surface_datasets()
            self.get_zone_datasets()
            self.post_processing()
        except ValueError as e:
            print("Exception:", e)
            print(f"...SQL output may not be up to date ({self.sqlfile})")
            print("...Try double checking idf")
            traceback.print_stack()
          
        
    def save(self, savedir): 
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        for name, df in self.zone_datasets.items():
            # Reset index so date is in the columns
            df_reset = df.reset_index()
            df_reset.to_csv(os.path.join(savedir, f'{name}.csv'), index=False)
        
        if self.run_surfaces:
            for name, df in self.surface_datasets.items():
                # Reset index so date is in the columns
                df_reset = df.reset_index()
                df_reset.to_csv(os.path.join(savedir, f'{name}.csv'), index=False)
    
    
    @staticmethod
    def get_zone_df(zone, df, name):
        df = df[df['KeyValue'] == zone]
        df = df['Value'].to_frame()
        df = df.rename(columns={'Value': name})
        return df
    
    
    def get_weather_dataset(self):
        weather_dfs = []
        for name, df in self.site_sql_dfs.items():
            try:
                weather_df = self.get_zone_df('Environment', df, name)
                weather_dfs.append(weather_df)
            except Exception as e:
                print('There was an error retrieving the weather: ', name)
                raise ValueError(e)
        self.weater_dataset = pd.concat(weather_dfs, axis=1)
       
    
    def get_zone_datasets(self):
        for zone in self.zones:
            zone_dfs, zone = [], zone[0]
            for name, df in self.zone_sql_dfs.items():
                try:
                    zone_df = self.get_zone_df(zone, df, name)
                    zone_dfs.append(zone_df)
                except Exception as e:
                    print('There was an error retrieving the zone: ', zone, name)
                    raise ValueError(e)
            # For analysis purposes, add the outdoor weather to each zone df
            zone_dfs.append(self.weater_dataset)
            zone_dataset = pd.concat(zone_dfs, axis=1)
            self.zone_datasets[zone] = zone_dataset
            
    def get_surface_datasets(self):
        if len(self.surface_output_vars) > 0:
            for surface in self.surfaces:
                surface_dfs, surface = [], surface[0]
                for name, df in self.surface_sql_dfs.items():
                    try:
                        surface_df = self.get_zone_df(surface, df, name)
                        surface_dfs.append(surface_df)
                    except Exception as e:
                        print('There was an error retrieving the surface: ', surface, name)
                        raise ValueError(e)
                surface_dataset = pd.concat(surface_dfs, axis=1)
                self.surface_datasets[surface] = surface_dataset
            
    def get_output_variables(self):
        st = time.time()
        for name in self.zone_output_vars:
            idf_name = zone_sql_measurements[name]
            df = select_time_series(self.sqlfile, step, name=idf_name)
            # print(idf_name, df['Units'].values[0])
            self.zone_sql_dfs[name] = df
            
        for name in self.site_output_vars:
            idf_name = site_sql_measurements[name]
            df = select_time_series(self.sqlfile, step, name=idf_name)
            # print(idf_name, df['Units'].values[0])
            self.site_sql_dfs[name] = df
            
        if self.run_surfaces:
            for name in self.surface_output_vars:
                idf_name = surface_sql_measurements[name]
                pdb.set_trace()
                df = select_time_series(self.sqlfile, step, name=idf_name)
                # print(idf_name, df['Units'].values[0])
                self.surface_sql_dfs[name] = df

#         print('getting output variables takes: ', time.time() - st)


    def post_processing(self):
        for zone, df in self.zone_datasets.items():
            df['total_s'] = df['diffuse_rate'] + df['direct_rate'] + df['ground_rate']
#             df.drop(columns=['diffuse_rate', 'direct_rate', 'ground_rate'], inplace=True)


### ================================================
### Extensions
### ================================================  
class TestGroundData(BuildingData):
    def __init__(self, sqlfile, run_surfaces):
        super().__init__(sqlfile, run_surfaces)
        self.surface_output_vars = [
            'outside_temp'
        ]
        eplus_zone_output_vars    = [zone_sql_measurements[x] for x in self.zone_output_vars]
        eplus_site_output_vars    = [site_sql_measurements[x] for x in self.site_output_vars]
        eplus_surface_output_vars = [surface_sql_measurements[x] for x in self.surface_output_vars]
        self.eplus_output_vars    = eplus_zone_output_vars + eplus_site_output_vars + eplus_surface_output_vars

    

class ElectricityPowerData(BuildingData):
    def __init__(self, sqlfile, run_surfaces):
        super().__init__(sqlfile, run_surfaces)
        self.zone_output_vars = self.zone_output_vars + [
            'lights_power', 'equip_power', 'occ_power', 
            'totalelec_power', 'totalhvac_power', 'totalbuil_power'
        ]
        eplus_zone_output_vars = [zone_sql_measurements[x] for x in self.zone_output_vars]
        eplus_site_output_vars = [site_sql_measurements[x] for x in self.site_output_vars]
        self.eplus_output_vars = eplus_zone_output_vars + eplus_site_output_vars


class TestLoadsData(BuildingData):
    def __init__(self, sqlfile, run_surfaces):
        super().__init__(sqlfile, run_surfaces)
        self.zone_output_vars = [
            'infloss_energy', 'infgain_energy', 'lights_power', 
            'occ_power', 'equip_power', 'ventchngs_ach'
        ]
        eplus_zone_output_vars = [zone_sql_measurements[x] for x in self.zone_output_vars]
        eplus_site_output_vars = [site_sql_measurements[x] for x in self.site_output_vars]
        self.eplus_output_vars = eplus_zone_output_vars + eplus_site_output_vars


class DynamicHLCData(BuildingData):
    def __init__(self, sqlfile, run_surfaces):
        super().__init__(sqlfile, run_surfaces)
        self.surface_output_vars = [
            'inside_temp', 'hgr_conv', 'hgr_rad', 'htc_conv', 
            'htc_rad_sky', 'htc_rad_air', 'hgr_cond_loss', 'htc_conv_in'
        ]
        eplus_zone_output_vars    = [zone_sql_measurements[x] for x in self.zone_output_vars]
        eplus_site_output_vars    = [site_sql_measurements[x] for x in self.site_output_vars]
        eplus_surface_output_vars = [surface_sql_measurements[x] for x in self.surface_output_vars]
        self.eplus_output_vars    = eplus_zone_output_vars + eplus_site_output_vars + eplus_surface_output_vars

# +
# class ElectricPowerDynamicHLC(BuildingData):
#     def __init__(self, sqlfile, run_surfaces):
#         super().__init__(sqlfile, run_surfaces)
#         self.zone_output_vars = self.zone_output_vars + [
#             'lights_power', 'equip_power', 'occ_power', 
#             'totalelec_power', 'totalhvac_power', 'totalbuil_power',
#             'infloss_energy', 'infgain_energy', 'ventchngs_ach', 'infmass',
#             'infrate_current', 'infrate_standard'
#         ]
#         self.surface_output_vars = [
#             'inside_temp', 'hgr_conv', 'hgr_rad', 'htc_conv', 
#             'htc_rad_sky', 'htc_rad_air', 'hgr_cond_loss', 'htc_conv_in',
#             'htc_rad_grd'
#         ]
#         eplus_zone_output_vars    = [zone_sql_measurements[x] for x in self.zone_output_vars]
#         eplus_site_output_vars    = [site_sql_measurements[x] for x in self.site_output_vars]
#         eplus_surface_output_vars = [surface_sql_measurements[x] for x in self.surface_output_vars]
#         self.eplus_output_vars    = eplus_zone_output_vars + eplus_site_output_vars + eplus_surface_output_vars


class ElectricPowerDynamicHLC(BuildingData):
    def __init__(self, sqlfile, run_surfaces):
        super().__init__(sqlfile, run_surfaces)
        self.zone_output_vars = self.zone_output_vars + [
            'equip_power', 'occ_power', 'ventchngs_ach',
            'infrate_current', 'infrate_standard'
        ]
        self.surface_output_vars = [
            'htc_conv', 'htc_rad_sky', 'htc_rad_air', 'htc_conv_in', 'htc_rad_grd'
        ]
        eplus_zone_output_vars    = [zone_sql_measurements[x] for x in self.zone_output_vars]
        eplus_site_output_vars    = [site_sql_measurements[x] for x in self.site_output_vars]
        eplus_surface_output_vars = [surface_sql_measurements[x] for x in self.surface_output_vars]
        self.eplus_output_vars    = eplus_zone_output_vars + eplus_site_output_vars + eplus_surface_output_vars
